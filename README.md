# CMEPDA Malandrone
![build status](https://gitlab.com/1998marcom/CMEPDA-Malandrone/badges/master/pipeline.svg)

Questo è il repo per la relazione sul progetto. Per il repo del progetto, si
guardi [segmentating-organs](https://gitlab.com/1998marcom/segmentating-organs).

L'ultima relazione è scaricabile
[qui](https://gitlab.com/1998marcom/CMEPDA-Malandrone/-/jobs/artifacts/master/raw/CMEPDA-Malandrone.pdf?job=compile_pdf)
