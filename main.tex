\documentclass[a4paper, 12pt]{article}

\usepackage[top=2.5cm,bottom=2.1cm,left=2cm,right=2cm]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{float}
\usepackage{tikz}

\title{ 
	Segmentation of organ tissues images\footnote{
		\url{https://gitlab.com/1998marcom/CMEPDA-Malandrone}
	}
	\\ \large Computing Methods for Experimental Physics and Data Analysis A 
} 

\date{\today} 

\author{Marco Malandrone}

\begin{document} 
\maketitle

\section{Introduction} 

The aim of this project is to annotate cell population
neighborhoods that perform an organ’s main physiologic function, also called
functional tissue units (FTUs). Manually annotating FTUs (e.g., glomeruli in
kidney or alveoli in the lung) is a time-consuming process. As an example, 
in the average kidney, there are over 1 million glomeruli FTUs.\\
This task, the process of partitioning images into multiple regions, is commonly
known as \textit{image segmentation}.

Furthermore, in this project we try to follow the philosophy of the original
kaggle competition\footnote{
	\url{https://www.kaggle.com/competitions/hubmap-organ-segmentation/}
} by using a single model for tissues from different organs.

\section{Data sources}
The data comes from the aforementioned kaggle competition. It consists of 351
images depicting sections of human organs tissues, as well as a
\texttt{train.csv} file that stores relevant metadata and the run-length encoded
segmentation masks.

The images span resolutions from 2308x2308 to 3070x3070, but are rescaled during
pre-processing to a fixed resolution. This fixed resolution is a tunable
hyperparameter. 

Data augmentations ideas are implemented in the image preprocessing using linear
transformations, elastic transforms\footnote{
	Simard, Steinkraus and Platt: "Best Practices for Convolutional
	Neural Networks applied to Visual Document Analysis", 2003
} and color transforms.

\section{The model: UNet}
The state-of-the-art models for medical image segmentation are variants of deep
convolutional neural networks with skip connections between layers,
called ``UNet''\footnote{
See the introduction of https://arxiv.org/abs/1912.05074.
Also, as an example, the first three open code notebooks in 
\url{https://kaggle.com/competitions/hubmap-kidney-segmentation/leaderboard}
are all using variations of UNets.
}, and variants of fully convolutional networks (FCN).

A graphical portrait of a small typical UNet is presented in figure
\ref{fig:smallunet}.

The model used in this project uses ``encoder'' and ``decoder'' as defined by
the following pseudo-code:
{
\footnotesize
\begin{verbatim}
# Encoder:
 (0): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
 (1): Conv2d(N, 2N, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=replicate)
 (2): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
 (3): ReLU()
 (4): Conv2d(2N, 2N, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=replicate)
 (5): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
 (6): ReLU()

# Decoder:
 (0): Conv2d(2N, 2N, kernel_size=(7, 7), stride=(1, 1), padding=(3, 3), padding_mode=replicate)
 (1): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
 (2): ReLU()
 (3): Conv2d(2N, 2N, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2), padding_mode=replicate)
 (4): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
 (5): ReLU()
 (6): PixelShuffle(upscale_factor=2)
\end{verbatim}
}

\section{Training}

The chosen loss function is a epoch-dependent function:
\begin{equation}
	L = \frac{a(e)\,L_\text{Dice}+b(e)\,L_\text{WCE}}{a(e)+b(e)}
\end{equation}
where:
\begin{itemize}
	\item $L_\text{Dice}$ is a regularization of $1-D$, where $D$ is the
		Dice coefficient,
	\item $L_\text{WCE}$ is the weighted cross entropy loss function,
	\item $a(e), b(e)$ are arbitrary functions of the epoch, chosen as:
		\begin{align*}
			a(e) &= 1\\
			b(e) &= \frac1{1+e}\,\text{lr\_decay\_rate}^e
		\end{align*}
\end{itemize}

The training has been performed with a stochastic gradient descent, with a 
decaying learning rate:
\begin{equation}
	\textrm{lr(epoch)} = \text{lr}(0) \;(1+\text{epoch})\; \text{lr\_decay\_rate}^\text{epoch}
\end{equation}

\section{Results}

The project code is available at \url{https://gitlab.com/1998marcom/segmenting-organs}.
The repo README contains the required information to reproduce the results obtained.
These results are obtained from commit
\texttt{6c29072b2954effb77f0be76a06171744b4984ab}, using the training 
hyperparameters in table \ref{tab:hyperpar}. 

The results show a test accuracy 
of 94.1\%, and a test Dice coefficient of 0.74.

By comparison, the median Dice coeffient in the current leaderboard of the running 
kaggle competition is 0.62.

A sample of the test case, with original images, target masks and predicted masks,
is provided in images \ref{fig:faccioschifo1} and \ref{fig:faccioschifo2}.


A model that has been used for comparison purposes is the Unet provided by the
segmentation\_models\_pytorch package, pretrained on the imagenet dataset and 
instanced with the arguments:
\begin{verbatim}
  encoder_name: resnet34
  encoder_depth: 5
  encoder_weights: imagenet
  decoder_channels: [512, 256, 128, 64, 32]
  decoder_attention_type: scse
  in_channels: 3
  classes: 2
  activation: "softmax"
\end{verbatim} 
After training, its results show a test accuracy of 94.9\% and a test Dice
coefficient of 0.75.

\section{Possible improvements}

The model here presented could be improved. Some ideas that can
be applied to this scenario are:
\begin{itemize}
	\item Tiling: original images were spanning  Here the issue of different resolutions was tackled by
		rescaling all images to a smaller one. 
		Another approach could be just cutting these images into
		fixed-resoultion tiles, with no other preprocessing (like scaling)
		necessary.
	\item Other datasets: a possible source of further improvement is widening
		the dataset, for example with data from 
		{\footnotesize \url{https://kaggle.com/competitions/hubmap-kidney-segmentation}}
		(huge image dimensions, not just resolutions, imply the need for tiling
		when using this kidney competition dataset).
	\item Trying to change the architecture of the network, by adding some
		layers skips, similiarly to the ResNet architecture, or by inserting
		attention layers.
	\item Using available metadata (data such as organ, sex, age,
		tissue thickness, real pixel size,...)
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{images/smallunet.png}
	\caption{
		Example of a small UNet
		{\footnotesize (awesomeopensource.com)}
	}
	\label{fig:smallunet}
\end{figure}

\vspace{50pt}

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		\textbf{hyperparameter} & \textbf{value}\\
		\hline
		resolution & 384x384 \\
		starting\_channels & 22 \\
		delta\_to\_maximum\_depth & 4 \\
		learning\_rate & 0.05 \\
		learning\_rate\_decay & 0.83 \\
  		epochs & 29 \\
		batch\_size & 7 \\
		\hline
	\end{tabular}
	\caption{Hyperparameters used}
	\label{tab:hyperpar}
\end{table}
\begin{figure}[H]
	\centering
	\foreach \x in {5,6,11,12,44} {
		\includegraphics[width=0.28\textwidth]{images/\x_x.png}
		\includegraphics[width=0.28\textwidth]{images/\x_pred.png}
		\includegraphics[width=0.28\textwidth]{images/\x_y.png}\\
	}
	\caption{Sample of the test case: original images, predictions, targets}
	\label{fig:faccioschifo1}
\end{figure}
\begin{figure}[H]
	\centering
	\foreach \x in {59,64,66,79,80} {
		\includegraphics[width=0.28\textwidth]{images/\x_x.png}
		\includegraphics[width=0.28\textwidth]{images/\x_pred.png}
		\includegraphics[width=0.28\textwidth]{images/\x_y.png}\\
	}
	\caption{Sample of the test case: original images, predictions, targets}
	\label{fig:faccioschifo2}
\end{figure}
\end{document}
